Document: express
Title: doxygen documentation for eXpress
Author: Adam Roberts, Lior Pachter
Abstract: Streaming quantification for high-throughput sequencing
 eXpress is a streaming tool for quantifying the abundances of a set of
 target sequences from sampled subsequences. Example applications include
 transcript-level RNA-Seq quantification, allele-specific/haplotype
 expression analysis (from RNA-Seq), transcription factor binding
 quantification in ChIP-Seq, and analysis of metagenomic data. It is
 based on an online-EM algorithm that results in space (memory)
 requirements proportional to the total size of the target sequences and
 time requirements that are proportional to the number of sampled
 fragments. Thus, in applications such as RNA-Seq, eXpress can accurately
 quantify much larger samples than other currently available tools
 greatly reducing computing infrastructure requirements. eXpress can be
 used to build lightweight high-throughput sequencing processing
 pipelines when coupled with a streaming aligner (such as Bowtie), as
 output can be piped directly into eXpress, effectively eliminating the
 need to store read alignments in memory or on disk.
Section: Science/Biology

Format: html
Files: /usr/share/doc/berkeley-express/html/*
Index: /usr/share/doc/berkeley-express/html/index.html
